Test UDP peer

##Example usage

Start 1st peer
```
node ./index.js 2222
```

Start 2nd peer and join the first
```
node ./index.js 2223 2222
```
