randon@aleena:~/workspace/tests/peerUDP$ cat peer.js 
'use strict';

const dgram = require('dgram');
const server = dgram.createSocket('udp4');

const from = s => `PeerUDP/${s}: `;
let seq = 0;

const sendJSON = (d, p, a) => {
    const us = server.address();
    seq++;
    d.seq = seq;
    console.log(`${from(`${us.address}:${us.port}`)}>> ${JSON.stringify(d)}`);
    server.send(JSON.stringify(d), p, a);
};

server.on('message', (pkt, info) => {
    const json = JSON.parse(pkt);
    console.log(`${from(`${info.address}:${info.port}`)}<< ${pkt.toString()}`);

    if (json.op === 'ack') return;

    // send ack
    sendJSON({
        op: 'ack',
        d: json.seq,
    }, info.port, info.address);
});

server.on('listening', () => {
    const us = server.address();
    console.log(`${from(`${us.address}:${us.port}`)}Listening`);

    if (process.argv[3]) {
        sendJSON({
            op: 'example',
            d: 'some data fool'
        }, process.argv[3], '127.0.0.1');
    }
});

server.bind(process.argv[2]);
