'use strict';

let i = 1;
let lastI = 1;
let lastNotif = Date.now();

let avg = [];

const calc = () => {
	// calc
	const rate = i - lastI;
	let years = (Number.MAX_SAFE_INTEGER / rate) / 60 / 60 / 24 / 30 / 12;
	if (years == 'Infinity') years = 0;

	avg.push([rate, years]);
	if (avg.length > 49) avg.shift();

	// calc avg
	const avgRate = Math.floor(avg.reduce((a, [rate, years]) => a + rate, 0) / avg.length);
	const avgYears = Math.floor(avg.reduce((a, [rate, years]) => a + years, 0) / avg.length);

	// write updates to console
	process.stdout.clearLine();
	process.stdout.cursorTo(0);
	process.stdout.write(`Rate: ${avgRate}/s, Years: ${avgYears}`);

	// update
	lastI = i;
	lastNotif = Date.now();
};
calc();

const fn = () => {
	i++;
	if (Date.now() - lastNotif > 1000) calc();
	if (i < Number.MAX_SAFE_INTEGER) process.nextTick(fn);
};
fn();
